# Meeting Scheduler System

System requirements for a fictitious meeting scheduler system

## High-level requirements

The following requirements describes high level system requirements

### [requirement id=REQ0001 quality=QR0003]

The system shall support creating a meeting event in a personal calendar.

### [requirement id=REQ0002]

The system shall support event labeling

## Detailed system requirements

The following requirements are derived from the above system level requirements

### [requirement id=REQ0003 parent=REQ0001 quality=QR0002 test=TC0001]

The system must provide the ability to schedule a meeting, event
or appointment with other calendar users.

### [requirement id=REQ0004 parent=REQ0001]

The system must provide option to set alerts/alarms/reminders
(i.e., audio or visual notification)

### [requirement id=REQ0005 parent=REQ0001]

The system should provide ability to forward alerts, alarms, or reminders to another device (e.g., PDA, Pager, Cell phone).

### [requirement id=REQ0006 parent=REQ0002 test=TC0003]

The system should allow user to label events with user defined
labels to support filtered or categorized views
