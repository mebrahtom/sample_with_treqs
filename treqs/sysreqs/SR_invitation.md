# Meeting Scheduler System

System requirements for a fictitious meeting scheduler system

## High-level requirements

The following text describes high level system requirements

### [requirement id=REQ0007]

The system shall support creating invitations to participants.

## Detailed system requirements

The following system requirements are derived from the above high level system requirement.

### [requirement id=REQ0008 parent=REQ0007]

The system must support delivery of invitations via e-mail.

### [requirement id=REQ0009 parent=REQ0007]

The system must account for time-zone adjustments when
inviting others.
